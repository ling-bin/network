# TCP-UDP网络组件

#### 介绍
tcp，tcp(tls)，udp 通讯的基础组件

#### 软件架构
这是一个 tcp，tcp(tls)，udp 通讯的基础组件，支持客户端，服务端，支持多分数据协议分包；


#### 使用说明
在目录apps中有对应示例
1.  Client.go  客户端使用示例，支持UDP，TCP通讯，支持多数据协议分包；
2.  TcpService.go TCP服务端使用示例，支持TCP通讯，支持多数据协议分包；
3.  TlsClient.go  TLS客户端使用示例，支持TLS安全连接通讯，支持多数据协议分包；
4.  TlsService.go TLS服务端使用示例，支持TLS安全连接通讯，支持多数据协议分包；
5.  UdpService.go UDP服务端使用示例，支持UDP通讯；
6.  Receivers  为分包器里面包含一个单字节分包器(JT808可以用)；
