package netInterface

//IReceiver 分包器
type IReceiver interface {
	GetHeadLen() int                                        //获取分包所需包头长度
	CanHandle(conn IConnection, buffer []byte) bool         //是否能处理
	Receiver(conn IConnection, buffer []byte) ([]byte, int) //返回数据包，处理到的位置
	Reset()                                                 //重置分包状态为初始状态[在Receiver内部异常时调用减少由于发送方发送数据有异常时包的丢失率]
}