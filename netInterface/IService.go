package netInterface

import (
	"time"
)

// IService 定义服务器接口
type IService interface {
	GetIsStart() bool                                                         //获取是否启动[true 启动，false 未启动]
	Start()                                                                   //启动服务器方法
	Stop()                                                                    //停止服务器方法
	GetConnMgr() IConnManager                                                 //得到链接管理
	GetConn(connId uint64) (IConnection, bool)                                //获取连接
	SetLogHandle(h func(level ErrLevel, msg ...interface{}))                  //设置内部异常抛出处理
	SetHandleStrategy(h func(IConnection, []byte) *StrategyData)              //设置处理策略,TCP和UDP表现不一样，tcp 只会在连接上第一包数据时调用，udp 会在每一包数上次都调用
	SetOnConnStart(h func(IConnection))                                       //设置连接开始处理方法
	SetOnConnStop(h func(IConnection))                                        //设置连接结束处理方法
	SetOnReceive(h func(IConnection, []byte))                                 //连接上传一包完整数据(如果配置 config.HDataCache 为true(默认true)时方法运行完成后会回收,不可再另开协程处理,如果要开启其它协程处理需要复制数据内容])
	SetOnReply(h func(IConnection, []byte, bool, string, interface{}, error)) //设置下发回调(连接,下发数据,是否成功,业务代码,下发数据时带的参数,异常信息)
	GetStartTime() time.Time                                                  //获取服务启动时间
	ToMap() map[string]int64                                                  //获取内部日志
}
