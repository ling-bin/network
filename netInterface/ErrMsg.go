package netInterface

import "errors"

var (
	ClosedErr = errors.New("连接关闭，不能发送消息")
	NotKey    = errors.New("扩展参数没有找到对应的Key")
)
