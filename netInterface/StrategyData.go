package netInterface

//StrategyData 策略数据
type StrategyData struct {
	/*
			业务唯一编号，注意该属性会设置到IConnection 扩展参数，业务需要避免覆盖属性

		    Udp会内部根据这个判断是否更换了连接，
			如果编号一样则没有更换，
			如果更换则认定为更换的连接
			内部会创建新连接对象 IConnection 的 ConnId 会更改

			TCP可以忽略不设置
	*/
	Key       string
	Receivers []IReceiver            //分包器，tcp使用，udp可以忽略
	ExtData   map[string]interface{} //扩展数据,会添加到 IConnection 的扩展数据中;
}
