package netInterface

import (
	"net"
	"time"
)

// IConnection 连接的接口
type IConnection interface {
	GetSession() interface{}                    //获取session 标志(给业务层使用)
	SetSession(session interface{})             //设置session 标志(给业务层使用)
	GetNetConn() interface{}                    //获取连接(udp获取到的是监听的连接，tcp获取的是真正的客户端连接)
	GetNetwork() string                         //获取网络类型[tcp,udp]
	GetConnId() uint64                          //获取客户端ID
	GetRemoteAddr() net.Addr                    //获取远程客户端地址信息
	GetLocalAddr() net.Addr                     //获取本地地址
	GetHeartTime() time.Time                    //连接最后一次接收数据时间[初始为连接建立时间]
	GetSendTime() time.Time                     //连接最后一次发送数据时间[初始化,调用接口,调用系统发送 的情况下会更新时间]
	GetStartTime() time.Time                    //连接建立时间
	SendData(data []byte, cmdCode string) error //发送消息到客户端: data 下发数据,cmdCode 指令标识[如: rep 普通回复, cmd 用户操作下发 。。]供业务使用
	// SendDataCall 发送消息到客户端带回调:data 下发数据,param 下发需要回调携带参数,cmdCode 指令标识[如: rep 普通回复, cmd 用户操作下发 。。]供业务使用,callFunc 下发后回调函数
	SendDataCall(data []byte, cmdCode string, param interface{}, callFunc func(IConnection, []byte, bool, string, interface{}, error)) error
	SetProperty(key string, value interface{})      //设置链接属性
	GetProperty(key string) (interface{}, error)    //获取链接属性
	RemoveProperty(key string)                      //移除链接属性
	GetPropertyKeys() []string                      //获取所有属性key
	GetRecInfo() (count, byteSize uint64)           //上行当前处理的包总数（处理前，1开始），总大小(字节)
	GetRepInfo() (count, byteSize, errCount uint64) //下行当前处理的包总数（处理后），总大小(字节)
	Stop()                                          //停止连接，结束当前连接状态
	GetIsClosed() bool                              //获取的状态（ture:关闭状态，false:未关闭）
	Incr(val int64) int64                           //连接提供给业务作为流水号使用,循环累加,(val 为正数为递增，val为负数为递减,val为0则获取值)
}
