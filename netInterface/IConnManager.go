package netInterface

/*IConnManager
  连接管理接口
*/
type IConnManager interface {
	Add(conn IConnection) bool                               //添加链接
	Remove(conn IConnection) bool                            //移除连接
	RemoveById(connId uint64) bool                           //移除连接
	Get(connId uint64) (IConnection, bool)                   //利用ConnID获取链接
	Count() int32                                            //获取个数
	Range(hFunc func(connId uint64, value IConnection) bool) //遍历
	ClearConn()                                              //删除并停止所有链接
}
