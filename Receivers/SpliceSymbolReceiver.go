package Receivers

import (
	"gitee.com/ling-bin/network/netInterface"
)

//SpliceSymbolReceiver 按照开始结束符来粘包(单字节版)
type SpliceSymbolReceiver struct {
	startSymbol  byte
	finishSymbol byte
}

// NewSpliceSymbolReceiver 按照开始结束符来粘包
func NewSpliceSymbolReceiver(startSymbol byte, finishSymbol byte) *SpliceSymbolReceiver {
	return &SpliceSymbolReceiver{
		startSymbol:  startSymbol,
		finishSymbol: finishSymbol,
	}
}

//GetHeadLen 包头必须要的长度
func (s *SpliceSymbolReceiver) GetHeadLen() int {
	return 1
}

//CanHandle 是否能处理
func (s *SpliceSymbolReceiver) CanHandle(conn netInterface.IConnection, buffer []byte) bool {
	return buffer[0] == s.startSymbol
}

//Reset 重置分包状态为初始状态
func (s *SpliceSymbolReceiver) Reset()  {

}

//Receiver 分包逻辑
func (s *SpliceSymbolReceiver) Receiver(conn netInterface.IConnection, buffer []byte) ([]byte, int) {
	var (
		bfLen      = len(buffer)
		startIndex = -1
		isStart    = false
	)
	for i := 0; i < bfLen; i++ {
		if !isStart && buffer[i] == s.startSymbol {
			startIndex = i
			isStart = true
		} else if buffer[i] == s.finishSymbol {
			hData := buffer[startIndex : i+1]
			startIndex = i + 1
			return hData, i + 1
		}
	}
	return nil, startIndex
}