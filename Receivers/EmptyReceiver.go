package Receivers

import "gitee.com/ling-bin/network/netInterface"

type EmptyReceiver struct {
	
}

//GetHeadLen 包头必须要的长度
func (s *EmptyReceiver) GetHeadLen() int {
	return 1
}

//CanHandle 是否能处理
func (s *EmptyReceiver) CanHandle(conn netInterface.IConnection, buffer []byte) bool {
	return true
}

//Reset 重置分包状态为初始状态
func (s *EmptyReceiver) Reset()  {

}

func (s *EmptyReceiver) Receiver(conn netInterface.IConnection, buffer []byte) ([]byte, int) {
	return buffer,len(buffer)
}
