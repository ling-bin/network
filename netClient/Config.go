package netClient

import (
	"crypto/tls"
	"time"
)

// Config 配置
type Config struct {
	Network         string        //网络类型：tcp,udp
	Host            string        //连接的IP和地址
	BufferSize      int           //缓存尺寸(字节)
	SendDataCount   int           //单个连接待发送数据个数[注：OverflowDiscard为true时，超过则丢弃]
	SendOutTime     time.Duration //下行超时时间(秒)
	OverflowDiscard bool          //接收，处理，回复溢出是否丢弃【true:丢弃，false：等待处理】
	IsTls           bool          //是否启用tls连接
	TlsConfig       *tls.Config   //tls配置
	SendRetryCount  int           //发送遇到临时错误失败重试次数
	HDataCache      bool          //默认 true, 注意：调用 SetOnReceive 的方法是否新申请内存[false:新申请内存，处理函数可以另外开启协程处理;true:不申请新内存，处理函数不可以另外开启协程处理]
}

// DefaultConfig 默认配置
func DefaultConfig(network, Host string) *Config {
	config := &Config{
		Network:         network,
		Host:            Host,
		BufferSize:      1024,
		SendDataCount:   128,
		SendOutTime:     time.Second * 5,
		IsTls:           false,
		OverflowDiscard: false,
		SendRetryCount:  3,
		HDataCache:      true,
	}
	return config
}

// NewConfig 默认配置
func NewConfig(network, Host string, tlsConfig *tls.Config, isTls bool) *Config {
	config := &Config{
		Network:        network,
		Host:           Host,
		BufferSize:     1024,
		SendDataCount:  128,
		SendOutTime:    time.Second * 5,
		IsTls:          isTls,
		TlsConfig:      tlsConfig,
		SendRetryCount: 3,
		HDataCache:     true,
	}
	return config
}
