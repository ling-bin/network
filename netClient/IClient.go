package netClient

import (
	"gitee.com/ling-bin/network/netInterface"
)

//IClient 客户端
type IClient interface {
	SetOnConnStart(hookFunc func(netInterface.IConnection))                                //设置连接开始事件
	SetOnConnStop(hookFunc func(netInterface.IConnection))                                 //设置连接停止事件
	SetOnReceive(hookFunc func(netInterface.IConnection, []byte))                          //设置数据包上传完成事件(如果配置 config.HDataCache 为true(默认true)时方法运行完成后会回收,不可再另开协程处理])
	SetHandleStrategy(h func(netInterface.IConnection, []byte) *netInterface.StrategyData) //设置处理策略,TCP和UDP表现不一样，tcp 只会在连接上第一包数据时调用，udp 会在每一包数上次都调用
	SetLogHandle(hookFunc func(level netInterface.ErrLevel, msg string))                   //设置内部异常处理
	SendData(data []byte, cmdCode string) error                                            //发送消息到客户端: data 下发数据,cmdCode 指令标识[如: rep 普通回复, cmd 用户操作下发 。。]
	//发送消息到客户端带回调:data 下发数据 param    下发需要回调携带参数 cmdCode  指令标识[如: rep 普通回复, cmd 用户操作下发 。。] callFunc 下发后回调函数
	SendDataCall(data []byte, cmdCode string, param interface{}, callFunc func(netInterface.IConnection, []byte, bool, string, interface{}, error)) error
	GetConn() netInterface.IConnection //获取连接
	Connect() (bool, error)            //连接
	Close()                            //关闭
}
