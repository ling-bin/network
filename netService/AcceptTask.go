package netService

import (
	"net"
	"sync"
)

// acceptTaskPool 连接处理对象池
var acceptTaskPool = sync.Pool{
	New: func() interface{} { return new(acceptTask) },
}

// acceptTask 连接接入任务
type acceptTask struct {
	Conn       net.Conn                 //连接
	ConnId     uint64                   //客户端ID
	addrConfig *AddrConfig              //服务地址
	OnAccept   func(accept *acceptTask) //接收到一次数据(未分包开始处理不确定是否完成)
}

// NewAcceptTask 创建接收对象
func newAcceptTask() *acceptTask {
	return acceptTaskPool.Get().(*acceptTask)
}

// free 回收释放
func (a *acceptTask) free() {
	a.Conn = nil
	a.ConnId = 0
	a.OnAccept = nil
	a.addrConfig = nil
	acceptTaskPool.Put(a)
}

// GetTaskId 获取连接id
func (a *acceptTask) GetTaskId() uint64 {
	return a.ConnId
}

// RunTask 处理数据
func (a *acceptTask) RunTask() {
	a.OnAccept(a)
	a.free()
}
