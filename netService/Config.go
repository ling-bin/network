package netService

import (
	"crypto/tls"
	"time"
)

const (
	connKey = "#ConnKey"  // 连接key[udp使用]
	HKey    = "#CallKey#" // 回调返回Key
)

// Config 配置
type Config struct {
	Network              string        //网络：tcp,udp,tls
	AddrAry              []*AddrConfig //[普通/Tls]监听地址和端口:绑定的IP加端口：["192.168.1.24:7018",...]
	ReceiveWorkerSize    uint          //(上行处理)工作池中工作线程个数,必须2的N次方
	ReceiveTaskQueueSize uint          //(上行处理)单个工作队列缓存任务大小
	ReplyWorkerSize      uint          //(下行处理)工作池中工作线程个数,必须2的N次方
	ReplyTaskQueueSize   uint          //(下行处理)单个工作队列缓存任务大小
	AcceptWorkerSize     uint          //(连接接入处理)工作池中工作线程个数[tcp使用],必须2的N次方
	AcceptTaskQueueSize  uint          //(连接接入处理)单个工作队列缓存任务大小[tcp使用]
	BufferSize           int           //缓存尺寸(字节)
	SendOutTime          time.Duration //(下行处理网络)超时时间
	OverflowDiscard      bool          //接收，处理，回复溢出是否丢弃【true:丢弃，false：等待处理】
	SendRetryCount       int           //发送遇到临时错误失败重试次数
	KeepTime             time.Duration //保持时间,(注：服务主要是是否能读取数据确认连接是否有存在必要)
	HDataCache           bool          //默认 true, 注意：调用 SetOnReceive 的方法是否新申请内存[false:新申请内存，处理函数可以另外开启协程处理;true:不申请新内存，处理函数不可以另外开启协程处理]
}

//CertFile   string             //server.crt 文件路径：TCP支持Tls
//KeyFile    string             //server.key 文件路径：TCP支持Tls
//ClientCer  string             //配置客户端证书【加入证书会开启客户端验证】
//ClientAuth tls.ClientAuthType //客户端验证类型

// AddrConfig Tls配置
type AddrConfig struct {
	Addr      string      //地址端口：192.168.1.24:7018
	TlsConfig *tls.Config //tls配置
	IsTls     bool        //是否开启tls
}

// DefaultConfig 普通网络配置
func DefaultConfig(network string, addrAry []string) *Config {
	tlsAddrAry := make([]*AddrConfig, 0, len(addrAry))
	for _, val := range addrAry {
		tlsAddrAry = append(tlsAddrAry, &AddrConfig{
			Addr:  val,
			IsTls: false,
		})
	}
	config := &Config{
		Network:              network,
		AddrAry:              tlsAddrAry,
		ReceiveWorkerSize:    512,
		ReceiveTaskQueueSize: 2048,
		ReplyWorkerSize:      512,
		ReplyTaskQueueSize:   1024,
		AcceptWorkerSize:     128,
		AcceptTaskQueueSize:  1024,
		BufferSize:           1024,
		SendOutTime:          time.Second * 5,
		OverflowDiscard:      false,
		SendRetryCount:       3,
		KeepTime:             time.Minute * 10,
		HDataCache:           true,
	}
	return config
}

// NewConfig 普通、Tls 网络都支持配置
func NewConfig(network string, addrAry []*AddrConfig) *Config {
	config := &Config{
		Network:              network,
		AddrAry:              addrAry,
		ReceiveWorkerSize:    512,
		ReceiveTaskQueueSize: 2048,
		ReplyWorkerSize:      512,
		ReplyTaskQueueSize:   1024,
		AcceptWorkerSize:     128,
		AcceptTaskQueueSize:  1024,
		BufferSize:           1024,
		SendOutTime:          time.Second * 5,
		OverflowDiscard:      false,
		SendRetryCount:       3,
		KeepTime:             time.Minute * 10,
		HDataCache:           true,
	}
	return config
}
