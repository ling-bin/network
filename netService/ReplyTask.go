package netService

import (
	"gitee.com/ling-bin/network/netInterface"
	"sync"
)

//replyTaskPool 回发处理对象池
var replyTaskPool = sync.Pool{
	New: func() interface{} { return new(replyTask) },
}

// replyTask 发送数据TCP
type replyTask struct {
	ConnId       uint64                                                                   //连接id
	Data         []byte                                                                   //发送数据
	Param        interface{}                                                              //参数
	CmdCode      string                                                                   //业务指定指令码
	CallFunc     func(netInterface.IConnection, []byte, bool, string, interface{}, error) //回调方法
	RunReplyTask func(replyTask *replyTask)                                               //下发完成回调方法
}

//newReplyTask 创建接收对象
func newReplyTask() *replyTask {
	return replyTaskPool.Get().(*replyTask)
}

// free 回收释放
func (r *replyTask) free() {
	r.Data = nil
	r.ConnId = 0
	r.Param = nil
	r.RunReplyTask = nil
	r.CallFunc = nil
	replyTaskPool.Put(r)
}

// GetTaskId 获取任务ID
func (r *replyTask) GetTaskId() uint64 {
	return r.ConnId
}

// RunTask 运行
func (r *replyTask) RunTask() {
	r.RunReplyTask(r)
	r.free()
}
